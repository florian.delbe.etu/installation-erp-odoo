#/bin/bash
config_ssh="ssh-conf"
config_interface="vm-conf/interface-conf"
config_ntp="vm-conf/ntp"
config_proxy="vm-conf/proxy"

echo "Création de odoo"
vmiut make odoo 1> /dev/null
echo "Création de bdd"
vmiut make bdd 1> /dev/null
echo "Création de saves"
vmiut make saves 1> /dev/null
echo "Démarrage de odoo"
vmiut start odoo 1> /dev/null
echo "Démarrage de bdd"
vmiut start bdd 1> /dev/null
echo "Démarrage de saves"
vmiut start saves 1> /dev/null

mkdir .ssh
ssh-keygen -f .ssh/identity
echo "Host *
           IdentityFile .ssh/identity" > $config_ssh

echo "Récupération des ips"
while ! [[ -n $odoo && -n $bdd && -n $saves ]]
do
    odoo=$(vmiut info odoo | tail -n1 | cut -d '=' -f 2)
    bdd=$(vmiut info bdd | tail -n1 | cut -d '=' -f 2)
    saves=$(vmiut info saves | tail -n1 | cut -d '=' -f 2)
done

echo "Ajout de bdd dans le fichier de configuration ssh"
echo "Host bdd
           HostName  $bdd
           User user" >> $config_ssh
echo "Ajout de saves dans le fichier de configuration ssh"
echo "Host saves
           HostName  $saves
           User user" >> $config_ssh
echo "Ajout de odoo dans le fichier de configuration ssh"
echo "Host odoo
           HostName  $odoo
           User user" >> $config_ssh

echo Copie de la clé ssh sur les machines virtuelles
id-ssh() {
    echo Copie sur $1
    ssh-copy-id -F $config_ssh $1
}
id-ssh "odoo"
id-ssh "bdd"
id-ssh "saves"

rootssh()
{
    echo "Mot de passe root de "$1" = 'root'"
    ssh -t -F $config_ssh $1 su root -c "cp\ -r\ /home/user/.ssh\ /root/.ssh"
}
rootssh "odoo"
rootssh "bdd"
rootssh "saves"
echo "Modification du fichier "$config_ssh
sed -Ei "s/user/root/g" $config_ssh

ch-root-passwd() {
    echo "Changez le mot de passe root de $1"
    ssh -t -F $config_ssh $1 passwd
}
ch-root-passwd "odoo"
ch-root-passwd "bdd"
ch-root-passwd "saves"

prohibit-password() {
    ssh -t -F $config_ssh $1 "sed -Ei \"s/#PermitRootLogin/PermitRootLogin/g\" /etc/ssh/sshd_config"
}
prohibit-password "odoo"
prohibit-password "bdd"
prohibit-password "saves"

change-port() {
    ssh -F $config_ssh $1 "sed -Ei \"s/#Port 22/Port 42069\"/g /etc/ssh/sshd_config"
    ssh -F $config_ssh $1 "systemctl restart sshd"
}
change-port "odoo"
change-port "bdd"
change-port "saves"
sed -i "2s/^/       Port 42069\n/g" $config_ssh

ipstatic () {
    echo "Configuration du Serveur NTP sur " $1
    scp -F $config_ssh $config_interface $1:/etc/network/interfaces.d/enp0s3
    ssh -F $config_ssh $1 'head -n8 /etc/network/interfaces > /etc/network/interfaces'
    ssh -F $config_ssh $1 sed -Ei 's/ADDRESS/$odoo/g' /etc/network/interfaces.d/enp0s3
}
ipstatic "odoo"
ipstatic "bdd"
ipstatic "saves"

echo "Configuration du proxy"
proxy() {
    scp -F $config_ssh $config_proxy $1:/etc/environment
}
proxy "odoo"
proxy "bdd"
proxy "saves"

ntp() {
    echo "Configuration du serveur NTP sur " $1
    ssh -F $config_ssh $1 systemctl stop systemd-timesyncd.service
    scp -F $config_ssh $config_ntp $1:/etc/systemd/timesyncd.conf
    ssh -F $config_ssh $1 systemctl start systemd-timesyncd.service
}
ntp "odoo"
ntp "bdd"
ntp "saves"

echo "Configuration des noms d'hôtes"
host() {
    ssh -F $config_ssh $1 "echo \"# Nom d'hôtes des autres machines virutelle\" >> /etc/hosts"
    if [[ $1 != "odoo" ]]
    then
        ssh -F $config_ssh $1 "echo \"$odoo     odoo\" >> /etc/hosts"
    fi
    if [[ $1 != "bdd" ]]
    then
        ssh -F $config_ssh $1 "echo \"$bdd      bdd\" >> /etc/hosts"
    fi
    if [[ $1 != "saves" ]]
    then
        ssh -F $config_ssh $1 "echo \"$saves      saves\" >> /etc/hosts"
    fi
    ssh -F $config_ssh $1 "sed -Ei \"s/debian/$1/g\" /etc/hosts"
    ssh -F $config_ssh $1 "hostname $1"
}
host "odoo"
host "bdd"
host "saves"

echo "Mise à jour des machines"
maj() {
    ssh -t -F $config_ssh $1 'apt-get update && apt-get -y upgrade'
}
maj "odoo"
maj "bdd"
maj "saves"

ssh -t -F $config_ssh bdd "apt-get install postgresql -y"
ssh -F $config_ssh bdd "echo \"host all all 192.168.194.0/24 md5\" >> /etc/postgresql/13/main/pg_hba.conf"
ssh -F $config_ssh bdd "sed -Ei \"s/#listen_addresses = 'localhost'/listen_addresses = 'localhost,$bdd'/g\" /etc/postgresql/13/main/postgresql.conf"
ssh -F $config_ssh bdd "systemctl restart postgresql"

echo "Création d'un admin Postgres "
read -p "Entrez un mot de passe : " -s psqlpasswd
sed -E "s/PASSWD/$psqlpasswd/g" psql/createadmin.sql > psql/admin.sql
scp -F $config_ssh psql/admin.sql bdd:/root/admin.sql
rm -f psql/admin.sql
ssh -F $config_ssh bdd "chmod o+x /root && chmod o+rx /root/admin.sql"
ssh -F $config_ssh bdd "su - postgres -c \"psql -f /root/admin.sql\""
ssh -F $config_ssh bdd "chmod o-x /root && rm /root/admin.sql"
ssh -F $config_ssh bdd "echo localhost:5432:postgres:padmin:$psqlpasswd > /home/user/.pgpass"
ssh -F $config_ssh bdd "chown user /home/user/.pgpass"
ssh -F $config_ssh bdd "chmod 0600 /home/user/.pgpass"

installdocker() {
    echo "Installation de Docker sur la machine " $1
    ssh -t -F $config_ssh $1 "wget -O - https://get.docker.com/ | sh"
    echo "Configuration de docker"
    ssh -F $config_ssh $1 "mkdir -p /etc/systemd/system/docker.service.d"
    scp -F $config_ssh docker/proxy-docker.conf $1:/etc/systemd/system/docker.service.d
    scp -F $config_ssh docker/daemon.json $1:/etc/docker/daemon.json
    ssh -F $config_ssh $1 systemctl daemon-reload
    ssh -F $config_ssh $1 systemctl restart docker
    ssh -F $config_ssh $1 "docker network create traefik_net"

}
installdocker "odoo"

installrsync() {
    echo "Installation de Rsync sur la machine $1"
    ssh -F $config_ssh $1 "apt-get install rsync -y"
}
installrsync "saves"
installrsync "bdd"

norootlogin() {
    ssh -t -F $config_ssh $1 "sed -Ei \"s/prohibit-password/no/g\" /etc/ssh/sshd_config"
}
norootlogin "odoo"
norootlogin "bdd"
norootlogin "saves"
sed -Ei "s/root/user/g" $config_ssh
sed -Ei "12s/user/root/g" $config_ssh

echo "Installation de Traefik"
scp -r -F $config_ssh traefik odoo:~/
ssh -F $config_ssh odoo "cd traefik && docker compose up -d"
echo "modification du fichier de configuration ssh"
cat >> $config_ssh << EOF
       LocalForward :9090 localhost:8080
       LocalForward :9091 localhost:8081
EOF

echo "Config SSH de saves"
ssh -F $config_ssh saves "mkdir .ssh"
ssh -F $config_ssh saves "ssh-keygen -f ~/.ssh/id_rsa -P ''"
ssh -t -F $config_ssh saves "ssh-copy-id -p 42069 user@bdd"

scp -F $config_ssh cron/bdd bdd:~/cron
scp -F $config_ssh cron/dump.sh bdd:~/dump.sh
scp -F $config_ssh cron/saves saves:~/cron
ssh -F $config_ssh bdd "mkdir ~/dumps"
ssh -F $config_ssh bdd "crontab ~/cron"
ssh -F $config_ssh saves "crontab ~/cron"
