#!/bin/bash

echo "Stop odoo"
vmiut stop odoo
echo "stop saves"
vmiut stop saves
echo "stop bdd"
vmiut stop bdd
echo "Supression de odoo"
vmiut rm odoo
echo "Supression de bdd"
vmiut rm bdd
echo "Supression de saves"
vmiut rm saves

echo "Supression du fichier de configuration ssh"
rm -f ssh-conf
rm -rf .ssh
