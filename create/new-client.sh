#/bin/bash
bdd=$(vmiut info bdd | tail -n1 | cut -d '=' -f 2)
[ $# -eq 0 ] && echo "Indiquez le fichier de configuration ssh" && exit 1
! [ -f $1 ] && echo "Le fichier n'existe pas" && exit 1
ssh_config=$1
read -p "Choix du port pour odoo (port 8069 par défaut) : " portodoo
[ -z $portodoo ] && portodoo=8069
read -p "Nom odoo (client1 est le nom par défaut) : " name
[ -z $name ] && name="client1"
read -p "Mot de passe odoo : " -s password
retrypassword() {
    echo "Le mot de passe ne peut pas être vide"
    read -p "Entrez un nouveau mot de passe : " -s password
}
while [ -z $password ] ; do retrypassword ; done

echo "Création du l'utilisateur et de la base de données $name"
mkdir -p $name/psql
sed -E "s/NAME/$name/g" psql/createuser.sql > $name/psql/odoo-$name.sql
sed -Ei "s/USERPASSWD/$password/g" $name/psql/odoo-$name.sql
scp -r -F $1 $name/psql bdd:~/$name
read -p "Entrez le mot de passe amdin : " -s padmin
ssh -F $1 bdd "echo localhost:5432:db_$name:padmin:$padmin >> ~/.pgpass"
ssh -t -F $1 bdd "psql -f ~/$name/odoo-$name.sql -h localhost -U padmin -d postgres"

echo "Création du fichier de configuration Odoo"
mkdir $name/odoo
mkdir -p $name/odoo/config
sed -E "s/POSTGRES/$bdd/g" odoo/odoo.conf > $name/odoo/config/odoo.conf
sed -Ei "s/USER/$name/g" $name/odoo/config/odoo.conf
sed -Ei "s/USRPASSWD/$password/g" $name/odoo/config/odoo.conf
sed -Ei "s/PORTODOO/$portodoo/g" $name/odoo/config/odoo.conf
sed -Ei "s/DBNAME/db_$name/g" $name/odoo/config/odoo.conf

echo "Création du service odoo pour $name"
sed -E "s/PORT/$portodoo/g" odoo/docker-compose.yml > $name/odoo/docker-compose.yml
sed -Ei "s/NAME/$name/g" $name/odoo/docker-compose.yml
sed -Ei "s/VIRT/$(hostname)/g" $name/odoo/docker-compose.yml
echo $bdd:5432:db_$name:$name:$password > $name/odoo/pg_pass
scp -F $1 -r $name/odoo odoo:~/$name
ssh -F $1 odoo "cd $name && docker compose up -d"
